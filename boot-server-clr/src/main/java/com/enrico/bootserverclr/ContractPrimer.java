package com.enrico.bootserverclr;

import com.enrico.bootserverclr.model.Contract;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class ContractPrimer implements CommandLineRunner {
    @Value("${amqp.queue.name}")
    private String queueName;

    private static final Logger LOGGER = LoggerFactory.getLogger(ContractPrimer.class);
    private RestTemplate restTemplate;
    private final RabbitTemplate rabbitTemplate;
    private final ConfigurableApplicationContext context;
    private final ObjectMapper objectMapper;

    @Autowired
    public ContractPrimer(RabbitTemplate rabbitTemplate, ConfigurableApplicationContext context, ObjectMapper objectMapper) {
        this.restTemplate = new RestTemplate();
        this.rabbitTemplate = rabbitTemplate;
        this.context = context;
        this.objectMapper = objectMapper;
    }

    @Override
    public void run(String... args) throws Exception {
        String url = "http://localhost:8080/api/contracts";
        Contract[] contracts = restTemplate.getForObject(url, Contract[].class);
        List<Contract> contractList = Arrays.asList(contracts);
        contractList.forEach(contract -> {
            LOGGER.info("Sending Contract: " + contract.toString());
            try {
                String jsonContractString = objectMapper.writeValueAsString(contract);
                rabbitTemplate.convertAndSend(queueName, jsonContractString);
            } catch (JsonProcessingException e) {
                LOGGER.error("Parsing Error: ", e.getMessage());
            }
        });
    }
}
