package com.enrico.bootservermessageconsumer;

import com.enrico.bootservermessageconsumer.model.Contract;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ContractMessageConsumer {
    private final ObjectMapper objectMapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(ContractMessageConsumer.class);

    @Autowired
    public ContractMessageConsumer(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void receiveContract(String jsonContract) {
        LOGGER.info("Contract Received");
        try {
            Contract contract = objectMapper.readValue(jsonContract, Contract.class);
            LOGGER.info("Contract received for review: " + contract.getSupplierId());
        }catch(IOException e){
            LOGGER.error("Errors: ", e.getLocalizedMessage());
        }
    }

}
