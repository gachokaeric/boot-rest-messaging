package com.enrico.bootservermessageconsumer.model;

public class Contract {
    private long ContractId;
    private int supplierId;
    private float amountPerDay;
    private float unitCost;
    private String status;

    public Contract() {
    }

    public Contract(long contractId, int supplierId, float amountPerDay, float unitCost, String status) {
        ContractId = contractId;
        this.supplierId = supplierId;
        this.amountPerDay = amountPerDay;
        this.unitCost = unitCost;
        this.status = status;
    }

    public long getContractId() {
        return ContractId;
    }

    public void setContractId(long contractId) {
        ContractId = contractId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public float getAmountPerDay() {
        return amountPerDay;
    }

    public void setAmountPerDay(float amountPerDay) {
        this.amountPerDay = amountPerDay;
    }

    public float getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(float unitCost) {
        this.unitCost = unitCost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "ContractId=" + ContractId +
                ", supplierId=" + supplierId +
                ", amountPerDay=" + amountPerDay +
                ", unitCost=" + unitCost +
                ", status='" + status + '\'' +
                '}';
    }
}
