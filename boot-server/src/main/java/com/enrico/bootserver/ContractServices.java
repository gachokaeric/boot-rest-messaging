package com.enrico.bootserver;

import com.enrico.bootserver.model.Contract;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContractServices {
    private static List<Contract> contractList = new ArrayList<>();

    static {
        for (int i = 0; i < 10; i++) {
            contractList.add(
                    new Contract(i, 10000000 + (int) (Math.random() * ((100000000 - 10000000) + 1)),
                            10 + (int) (Math.random() * ((50 - 10) + 1)),
                            30 + (int) (Math.random() * ((50 - 30) + 1)),
                            "pending")
            );
        }
    }

    public List<Contract> getContracts() {
        return contractList;
    }
}
