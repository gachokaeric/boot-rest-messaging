package com.enrico.bootserver;

import com.enrico.bootserver.model.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ApiController {
    private ContractServices contractServices;

    @Autowired
    public ApiController(ContractServices contractServices) {
        this.contractServices = contractServices;
    }

    @GetMapping("/contracts")
    public List<Contract> getContracts() {
        return contractServices.getContracts();
    }
}
